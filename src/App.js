import './App.css';
import React, {useState} from 'react';

function App() {
    // 예산 처리
    const [price, setPrice] = useState('')
    // 근사치 차량
    const [nearbyCar, setNearbyCar] = useState('')
    // 에러 처리
    const [error, setError] = useState('')


    const carData = [
        {name: "승용차1", carPrice: 20000000},
        {name: "승용차2", carPrice: 30000000},
        {name: "승용차3", carPrice: 40000000},
        {name: "승용차4", carPrice: 50000000},
        {name: "승용차5", carPrice: 60000000},
    ]

    const handlePriceChange = e => {
        // 예산 초기화
        setPrice(e.target.value)
        // 접근한 차량 초기화
        setNearbyCar('')
        // 에러 초기화
        setError('')
    }

    const calculateCarPrice = () => {
        // 조절한 비용 = (입력한 예산 - (보험료 + 공채 + 탁송료)) / 1 + 0.07(취등록세)
        // 조절한 비용은 부가비용을 뺀 차량을 살 수 있는 예산
        const adjustedPrice = (price - 4000000) / 1.07
        // 현재 근접한 차량 = 차 데이터 index 0번
        let nearbyCar = carData[0]
        // 최소 차이 = 절대값(0번 차량 가격 - 조절한 비용)
        // 값이 작을수록 입력한 예산이 적다는 것
        let minDiff = Math.abs(carData[0].carPrice - adjustedPrice)

        for (let i = 1; i < carData.length; i++) {
            // 차이 = 절대값(i번 차량 가격 - 조절한 비용)
            const diff = Math.abs(carData[i].carPrice - adjustedPrice)
            // 만약에 차이 < 최소 차이이면서 i번 차량의 가격이 조절한 비용과 작거나 같다면
            if (diff < minDiff && carData[i].carPrice <= adjustedPrice) {
                // 최소 차이 = 차이
                minDiff = diff
                // 현재 근접한 차량 = i번 차량
                nearbyCar = carData[i]
            }
        }

        // 현재 근접한 차량의 가격이 입력한 예산보다 크다면
        if (nearbyCar.carPrice > price) {
            // 돈이 부족하면 뜨는 에러
            setError('돈이 부족하당')
        } else {
            // 근접한 차량에 현재 근접한 차량 이름 넣기
            setNearbyCar(nearbyCar.name)
        }
    }


    return (
        <div className="App">
            <h1>내 예산에 맞는 자동차 추천</h1>
            <input type="number" value={price} onChange={handlePriceChange}></input>
            <button onClick={calculateCarPrice}>입력</button>
            {error ? <p>{error}</p> : <p>추천차량 : {nearbyCar}</p>}
        </div>
    )
}

export default App
